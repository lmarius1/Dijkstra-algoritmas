import javafx.scene.control.Alert;
import javafx.stage.Stage;

public class AlertBox {
    public AlertBox(String message, Stage window){
        alert(message, window);
    }

    public AlertBox(String message, Stage window, boolean kitaValue){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Klaida");
        alert.setHeaderText(message);

        alert.setY(window.getY() + 130);
        alert.setX(window.getX() + 35);
        // + window.getWidth()/2 - 150
        alert.showAndWait();
    }


    public void alert(String message, Stage window){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Klaida");
        alert.setHeaderText(message);

        alert.setY(window.getY() + 130);
        alert.setX(window.getX() + 35);
        // + window.getWidth()/2 - 150
        alert.showAndWait();
    }
}
