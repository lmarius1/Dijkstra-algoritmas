import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.PriorityQueue;

public class Dijkstra {
    static final int max_val = Integer.MAX_VALUE;

    public void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int t = Integer.parseInt(reader.readLine());
        while (t-- > 0){
            String[] inStr = reader.readLine().split(" ");
            int n = Integer.parseInt(inStr[0]);
            int m = Integer.parseInt(inStr[1]);
            int[][] w = new int[n][n];
            for (int i = 0; i < n; i++)
                for (int j = 0; j < n; j++)
                    w[i][j] = -1;
            for (int i = 0; i < m; i++) {
                inStr = reader.readLine().split(" ");
                int x = Integer.parseInt(inStr[0]) - 1;
                int y = Integer.parseInt(inStr[1]) - 1;
                int r = Integer.parseInt(inStr[2]);
                if (w[x][y] == -1 || w[x][y] > r)
                    w[x][y] = w[y][x] = r;
            }
            int s = Integer.parseInt(reader.readLine()) - 1;
            print(solveDijkstra(w, s), s);
        }
        reader.close();
    }

    public Vertex[] solveDijkstra(int[][] w, int s){
        //w - grafas, kur pirma masyvo dalis int[x][] yra skirta Vertex. O kita int[][y] yra edges (jei int[][y] nera edge, tai value turi buti -1 ten).
        //int s - kurioje vietoje prasideda grafas
        //int n - masyvo dydis
        int n = w.length;
        PriorityQueue<Vertex> vertices = new PriorityQueue<>(n);
        Vertex[] v = new Vertex[n];
        for (int i = 0; i < n; i++) {
            Vertex vertex = new Vertex(i);
            if (i == s)
                vertex.setW(0);
            vertices.add(vertex);   //n times Log(n), kadangi prideti i priority queue yra Log(n), o einama n times.
            v[i] = vertex;
        }
        while (!vertices.isEmpty()) {
            //cia bus atlikta n times, kur n - vertex
            Vertex u = vertices.remove();   //Log(n)
            if(u.getW() == Integer.MAX_VALUE)   //kai yra imamas su MAX_VALUE value, tai jau kiti like irgi bus, tad tiesiog MAX_VALUE...
                break;
            for (int i = 0; i < n; i++) {
                if (w[u.ind][i] != -1 && (v[i].getW() > u.getW() + w[u.ind][i])) {
                    v[i].setW(u.getW() + w[u.ind][i]);
                    vertices.remove(v[i]);  //jei jau yra tame priorityQueue, tai reikia istrinti, kad idetu i tinkama vieta
                    vertices.add(v[i]);
                    v[i].road = new ArrayList<>();
                    for(int q : u.road) v[i].road.add(q);
                    v[i].road.add(u.ind);
                }
            }
        }
        return v;
        //grazinami vertices su tam tikromis reiksmemis ir keliu iki ju.
    }
    public void print(Vertex[] v, int s){
        //s - start position
        for (int i = 0; i < v.length; i++) {
            if (i == s)
                continue;
            if (v[i].getW() == max_val)
                System.out.println(i + ": nėra kelio. ");
            else {
                System.out.print(i + ":  ");
                for(int j : v[i].road){
                    System.out.print(j + " -> ");
                }
                System.out.println(i + "  = " + v[i].getW());
//                System.out.print(v[i].getW() + " ");
            }
        }
        System.out.println();
    }
}

