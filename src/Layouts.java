import com.sun.javafx.tk.FontLoader;
import com.sun.javafx.tk.Toolkit;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.ArrayList;

public class Layouts {
    private Stage window;
    public Layouts(Stage window){
        this.window = window;
    }

    public void loadScreen2() {
        window.setTitle("Dijkstra");
        Label lbTitle = new Label("Užpildykite grafo matricinę išraišką");
        lbTitle.setAlignment(Pos.CENTER);
        lbTitle.setId("title");
        Button btnClear = new Button("Išvalyti");
        Button btnSkaiciuoti = new Button("Skaičiuoti");
        btnClear.setMinWidth(70);
        btnSkaiciuoti.setMinWidth(70);
        Button btnMinus = new Button("-");
        Button btnPlus = new Button("+");
        btnMinus.setMinWidth(30);
        btnPlus.setMinWidth(30);

        ComboBox<String> comboBoxPasirinkimas = new ComboBox<>();
        comboBoxPasirinkimas.getItems().addAll("0", "1", "2");
        comboBoxPasirinkimas.setValue("0");
        comboBoxPasirinkimas.setMinWidth(85);
        VBox vPasir = new VBox(5);
        vPasir.setAlignment(Pos.BOTTOM_LEFT);
        vPasir.getChildren().addAll(new Label("Pradžios viršūnė"), comboBoxPasirinkimas);

        HBox hBox = new HBox(10);
        hBox.setAlignment(Pos.BOTTOM_CENTER);
        hBox.getChildren().addAll(btnMinus, btnPlus, btnClear, btnSkaiciuoti, vPasir);

        BorderPane borderPane = new BorderPane();
        VBox vbTop = new VBox(10);
        vbTop.setAlignment(Pos.CENTER);
        vbTop.setPadding(new Insets(15, 0, 0, 0));
        vbTop.getChildren().addAll(lbTitle, hBox);
        borderPane.setTop(vbTop);

        ArrayList<ArrayList<TextField>> fields = createTextField(3);
        HBox fieldsSP = createLinearTable(fields);

        HBox answerHBox = new HBox(); //Kai pridedamas answer, bus dedama prie center VBox.
        answerHBox.setAlignment(Pos.TOP_CENTER);

        VBox centerVBox = new VBox(30); //sis VBox laikys tiesiniu lygciu sistemos langelius ir taip pat atsakymus.
        centerVBox.getChildren().addAll(fieldsSP, answerHBox);
        borderPane.setCenter(centerVBox);

        Dijkstra dijkstra = new Dijkstra(); //tiesiog is anksto sukuriamas objektas skaiciavimui
        btnPlus.setOnAction(e -> {
            //cia turetu buti padaryta taip, jog pasiimciau kiekviena hbox line ir pridedama butu po paskutini tf
            //ir galiausiai dar vienas tf turi buti pridedamas.

            addLineToTF(fields, comboBoxPasirinkimas);
            centerVBox.getChildren().clear();
            centerVBox.getChildren().add(createLinearTable(fields));
            System.gc();    //callinamas garbage collector, kad ismestu nereikalinga memory.
        });
        btnMinus.setOnAction(e -> {
            removeLineFromTF(fields, comboBoxPasirinkimas);
            centerVBox.getChildren().clear();
            centerVBox.getChildren().add(createLinearTable(fields));
            System.gc();
        });
        btnClear.setOnAction(e -> {
            clearFields(fields);
        });
        btnSkaiciuoti.setOnAction(e -> {
            int s = 0;
            try{
                s = Integer.parseInt(comboBoxPasirinkimas.getValue());
            } catch (Exception exc){
                new AlertBox(exc.getMessage(), window);
                e.consume();
            }
            //getIntArray(fields);
            //dijkstra.print(dijkstra(getIntArray(fields), 1), 1);
            Vertex[] v = dijkstra.solveDijkstra(getIntArray(fields), s);
            VBox answerVB = convertVerticesToVB(v, s);
            centerVBox.getChildren().remove(answerHBox);
            answerHBox.getChildren().clear();
            answerHBox.getChildren().add(answerVB);
            centerVBox.getChildren().add(answerHBox);
        });

        Scene scene = new Scene(borderPane);
        scene.getStylesheets().addAll("JavaFXCSSConfig.css");
        window.setScene(scene);
        window.show();
    }

    private VBox convertVerticesToVB(Vertex[] v, int s){
        Label answerTitle = new Label("Rezultatai");
        answerTitle.setAlignment(Pos.CENTER);
        answerTitle.setId("titleRez");
        VBox vb = new VBox(5);
        vb.getChildren().add(answerTitle);
        for (int i = 0; i < v.length; i++) {
            if (i == s)
                continue;
            HBox hb = new HBox(3);
            Label lbVertex = new Label(i + ": ");
            lbVertex.setId("rez");
            hb.getChildren().add(lbVertex);
            if (v[i].getW() == Integer.MAX_VALUE) {
                Label label = new Label("nėra kelio");
                label.setId("rez");
                hb.getChildren().add(label);
            }
            else {
                for(int j : v[i].road){
                    Label label = new Label(j + " -> ");
                    label.setId("rez");
                    hb.getChildren().add(label);
                }
                Label lbEquals = new Label(i + "  = " + v[i].getW());
                lbEquals.setId("rez");
                hb.getChildren().add(lbEquals);
            }
            vb.getChildren().add(hb);
        }
        return vb;
    }

    private void clearFields(ArrayList<ArrayList<TextField>> fields) {
        int n = fields.size();
        for(int i = 0; i < n; i++){
            for(int j = 0; j < n; j++) {
                fields.get(i).get(j).clear();
            }
        }
    }
    private int[][] getIntArray(ArrayList<ArrayList<TextField>> fields){
        int n = fields.size();
        int[][] arr = new int[n][n];
        for(int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                arr[i][j] = -1;
        for(int i = 0; i < n; i++){
            for(int j = 0; j < n; j++){
                String tfStr = fields.get(i).get(j).getText();
                if (tfStr.compareTo("") != 0) {
                    try {
                        arr[i][j] = Integer.parseInt(fields.get(i).get(j).getText());
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                }
            }
        }
        return arr;
    }

    public ArrayList<ArrayList<TextField>> createTextField(int kiek){
        ArrayList<ArrayList<TextField>> fields = new ArrayList<>();
        for(int i = 0; i < kiek; i++){
            ArrayList<TextField> arr = new ArrayList<>();
            for(int j = 0; j < kiek; j++){
                TextField tf = new TextField() {
                    @Override public void replaceText(int start, int end, String text) {
                        if (text.matches("[0-9]*")) {
                            super.replaceText(start, end, text);
                        }
                    }
                    @Override public void replaceSelection(String text) {
                        if (text.matches("[0-9]*")) {
                            super.replaceSelection(text);
                        }
                    }
                };
                tf.setPrefWidth(50);
                tf.setMaxWidth(50);

                if(i == j){
                    tf.setId("colorDiagonal");
                }

                arr.add(tf);
            }
            fields.add(arr);
        }
        return fields;
    }

    public void removeLineFromTF(ArrayList<ArrayList<TextField>> fields, ComboBox<String> startPoints){
        int n = fields.size();
        if(n <= 2){
            return;
        }
        startPoints.getItems().remove(n-1); //jei yra 0,1,2 ir trinamas 2, tai n = 3, o man reikia trinti 2.
        for(int i = 0; i < n-1; i++){   //istrinami visur paskutiniai eiluciu elementai
            fields.get(i).remove(n-1);
        }
        fields.remove(n-1);
    }

    public void addLineToTF(ArrayList<ArrayList<TextField>> fields, ComboBox<String> startPoints){
        int n = fields.size();
        if(n > 25){
            return;
        }
        startPoints.getItems().addAll(Integer.toString(n));
        for(ArrayList<TextField> tfs : fields){
            TextField tf = new TextField() {
                @Override public void replaceText(int start, int end, String text) {
                    if (text.matches("[0-9]*")) {
                        super.replaceText(start, end, text);
                    }
                }
                @Override public void replaceSelection(String text) {
                    if (text.matches("[0-9]*")) {
                        super.replaceSelection(text);
                    }
                }
            };
            tf.setPrefWidth(50);
            tf.setMaxWidth(50);

            tfs.add(tf);
        }
        ArrayList<TextField> arr = new ArrayList<>();
        for(int i = 0; i < n+1; i++){
            TextField tf = new TextField() {
                @Override public void replaceText(int start, int end, String text) {
                    if (text.matches("[0-9]*")) {
                        super.replaceText(start, end, text);
                    }
                }
                @Override public void replaceSelection(String text) {
                    if (text.matches("[0-9]*")) {
                        super.replaceSelection(text);
                    }
                }
            };

            tf.setPrefWidth(50);
            tf.setMaxWidth(50);

            if(i == n){
                tf.setId("colorDiagonal");
            }
            arr.add(tf);
        }
        fields.add(arr);
    }

    public HBox createLinearTable(ArrayList<ArrayList<TextField>> fields){
        int n = fields.size();
        VBox finalTable = new VBox(10);
        HBox firstLineLabels = new HBox(3);
        for(int i = 0; i < n; i++){
            Label label = new Label(Integer.toString(i));
            label.setId("boldText");
            label.setMinWidth(50);
            label.setAlignment(Pos.BOTTOM_CENTER);
            firstLineLabels.getChildren().add(label);
        }

        FontLoader fontLoader = Toolkit.getToolkit().getFontLoader();
        Label l = new Label(Integer.toString(n-1));
        l.setId("boldText");
        double padding = fontLoader.computeStringWidth(l.getText(), l.getFont());   //determine the width of the row labels
        firstLineLabels.setPadding(new Insets(0,0,-10,padding+3));  //set padding according to the row labels

        finalTable.getChildren().addAll(firstLineLabels);

        for(int i = 0; i < n; i++){
            HBox hb = new HBox(3);
            hb.setAlignment(Pos.CENTER);
            Label label = new Label(Integer.toString(i));
            label.setId("boldText");
            label.setAlignment(Pos.CENTER);
            hb.getChildren().add(label);
            for(int j = 0; j < n-1; j++){
                hb.getChildren().addAll(fields.get(i).get(j));
            }
            hb.getChildren().addAll(fields.get(i).get(n-1));
            finalTable.getChildren().addAll(hb);
        }
        ScrollPane sp = new ScrollPane(finalTable);
        sp.setStyle("-fx-background-color:transparent;");
        HBox hbR = new HBox(sp);
        hbR.setAlignment(Pos.CENTER);
        hbR.setPadding(new Insets(15, 0, 0, 0));
        return hbR;
    }
}
