import java.util.ArrayList;

/**
 * Created by Marius Lk on 2016-12-09.
 */
public class Vertex implements Comparable<Vertex> {
    int ind;
    private int w = Integer.MAX_VALUE;
    ArrayList<Integer> road;  //road to the Vertex

    public Vertex(int ind) {
        this.ind = ind;
        road = new ArrayList<>();
    }

    public int getW() {
        return w;
    }

    public void setW(int w) {
        this.w = w;
    }

    @Override
    public int compareTo(Vertex o) {
        return w - o.w;
    }
}
